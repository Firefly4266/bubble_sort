'use strict';

let array = [];
for(let i = 0; i < 15; i++) {
  array[i] = i;
}

let shuffle = function(array) {
  let temp, current, top = array.length;
  current = Math.floor(Math.random() * (top + 1));
  if(top) {
    while(--top) {
      temp = array[current];
      array[current] = array[top];
      array[top] = temp;
    }
    return array;
  }
};

array = shuffle(array);

console.log(array);


//<-----------------bubble sort------------------>

let bubbleSort = function() {
  let length = array.length;
  for(let i = 0; i < length; i++) {

    //by adding - i after the length we prevent comparing the numbers that have already been sorted at the end.
    for(let j = 0; j < length - 1 - i; j++) {
      if(array[j] > array[j + 1]) {
        swap(array, j, j + 1);
      }
    }
  }
};

let swap = function(array, index1, index2) {
  let temp = array[index1];
  array[index1] = array[index2];
  array[index2] = temp;
};

bubbleSort();
console.log(array);
