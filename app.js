'use strict';

let array = [];
for(let i = 0; i < 20; i++) {
  array[i] = i;
}

let shuffle = function(array) {
  let temp, current, top = array.length;
  current = Math.floor(Math.random() * (top + 1));
  if(top) {
    while(--top) {
      temp = array[current];
      array[current] = array[top];
      array[top] = temp;
    }
    return array;
  }
};

array = shuffle(array);

console.log(array);

let bubbleSort = function() {
  let length = array.length;
  for(let i = 0; i < length; i++) {
    for(let j = 0; j < length - 1 - i; j++) {
      if(array[j] > array[j + 1]) {
        swap(array, j, j + 1);
      }
    }
  }
};

let swap = function(array, index1, index2) {
  let temp = array[index1];
  array[index1] = array[index2];
  array[index2] = temp;
};

bubbleSort();
console.log(array);
